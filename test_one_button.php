<?php

/**
 * @file
 */

/*** VARIABLES USED IN APPLICATION ***/

// Initialization Vector, randomly generated and saved each time
// Note: This does not need to be kept secret.
$iv = substr(sha1(mt_rand()), 0, 16);

// Salt: this should be stored somewhere in database and retrieved, this should not be in the code.
$salt = '';

// Username that has to ability to post on the UWaterloo homepage, this should be retreived from a database and not stored in code.
$username = '';

// Password for the user that has the ability to post on the UWaterloo homepage, this should be retieved from a database and not stored in code.
$password = '';

// The URL for the special alert POST, this should be stored in a database and not stored in code.
$post_special_alert_url = '';

// The URL used to pull the current special alert.  This should be stored in a database and not in code.
$pull_special_alert_url = '';

// The URL for the special alert POST, this should be stored in a database and not stored in code.
$post_major_alert_url = '';

// The URL used to pull the current special alert.  This should be stored in a database and not in code.
$pull_major_alert_url = '';

/*** END VARIABLES USED IN APPLICATION ***/

// If the submit button is pushed on the form.
if (isset($_POST['special_alert_submit_button'])) {

  // Ensure that all fields are filled out.
  if ($_POST['special_alert_title'] == "" || $_POST['special_alert_body'] == "" || $_POST['special_alert_status'] == "") {
    $special_alert_message = 'Please ensure all fields are completed';
  }
  else {

    // Data to send to the POST.
    $data = array(
      'username' => openssl_encrypt($username, 'aes-256-cbc', $salt, NULL, $iv),
      'password' => openssl_encrypt($password, 'aes-256-cbc', $salt, NULL, $iv),
      'title' => $_POST['special_alert_title'],
      'body' => $_POST['special_alert_body'],
      'status' => $_POST['special_alert_status'],
      'iv' => $iv,
    );

    // cURL: Posting to the homepage page.
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $post_special_alert_url);
    // Do a regular HTTP POST.
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    // Set POST data.
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    // Ask to not return Header.
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FAILONERROR, 1);

    $response = curl_exec($curl);
    $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    print_r($response);
    print_r($http_code);
  }
}

// If the submit button is pushed on the form.
if (isset($_POST['major_alert_submit_button'])) {

  // Ensure that all fields are filled out.
  if ($_POST['major_alert_title'] == "" || $_POST['major_alert_body'] == "" || $_POST['major_alert_status'] == "") {
    $major_alert_message = 'Please ensure all fields are completed';
  }
  else {

    // Data to send to the POST.
    $data = array(
      'username' => openssl_encrypt($username, 'aes-256-cbc', $salt, NULL, $iv),
      'password' => openssl_encrypt($password, 'aes-256-cbc', $salt, NULL, $iv),
      'title' => $_POST['major_alert_title'],
      'body' => $_POST['major_alert_body'],
      'sidebar' => $_POST['major_alert_sidebar'],
      'status' => $_POST['major_alert_status'],
      'iv' => $iv,
    );

    // cURL: Posting to the homepage page.
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $post_major_alert_url);
    // Do a regular HTTP POST.
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    // Set POST data.
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    // Ask to not return Header.
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FAILONERROR, 1);

    $response = curl_exec($curl);$response = curl_exec($curl);
    $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    print_r($response);
    print_r($http_code);
  }
}

/*** SPECIAL ALERT PULL ***/

// Initiate curl.
$ch = curl_init();
// Disable SSL verification.
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
// Will return the response, if false it print the response.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
// Set the url.
curl_setopt($ch, CURLOPT_URL, $pull_special_alert_url);
// Execute.
$special_alert_result = curl_exec($ch);
// Closing.
curl_close($ch);

// The variabled that will stored the resulted json data.
$special_alert = json_decode($special_alert_result, TRUE);

/*** MAJOR ALERT PULL ***/

// Initiate curl.
$ch = curl_init();
// Disable SSL verification.
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
// Will return the response, if false it print the response.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
// Set the url.
curl_setopt($ch, CURLOPT_URL, $pull_major_alert_url);
// Execute.
$major_alert_result = curl_exec($ch);
// Closing.
curl_close($ch);

// The variabled that will stored the resulted json data.
$major_alert = json_decode($major_alert_result, TRUE);

if (isset($special_alert_message)) {
  print $special_alert_message;
}
?>
<form id="special_alert_form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
  <b><u>Special Alert</u></b>
  <br /><br />
  Title:<br />
  <input type="text" name="special_alert_title" value="<?php
  if (isset($special_alert['data'][0]['title'])) {
    print $special_alert['data'][0]['title'];
  }
  ?>" />
  <br /><br />
  Body:<br />
  <textarea name="special_alert_body" cols="60" rows="10"><?php
  if (isset($special_alert['data'][0]['body'])) {
    print $special_alert['data'][0]['body'];
  }
  ?></textarea>
  <br /><br />
  Status:<br />
  <select name="special_alert_status">
    <?php if (isset($special_alert['data'][0]['status'])) : ?>
      <?php if ($special_alert['data'][0]['status'] == 1) : ?>
        <option value="1" selected="selected">Publish</option>
      <?php else : ?>
        <option value="1">Publish</option>
      <?php endif; ?>
    <?php else : ?>
      <option value="1">Publish</option>
    <?php endif; ?>

    <?php if (isset($special_alert['data'][0]['status'])) : ?>
      <?php if ($special_alert['data'][0]['status'] == 0) : ?>
        <option value="0" selected="selected">Unpublish</option>
      <?php else : ?>
        <option value="0">Unpublish</option>
      <?php endif; ?>
    <?php else : ?>
      <option value="0">Unpublish</option>
    <?php endif ?>
  </select>
  <br /><br />
  <input type="submit" name="special_alert_submit_button" value="Submit">
</form>

<br /><br />

<?php
if (isset($major_alert_message)) {
  print $major_alert_message . '<br />';
}
?>

<form id="alert_form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
  <b><u>Major Alert</u></b>
  <br /><br />
  Title:<br />
  <input type="text" name="major_alert_title" value="<?php
  if (isset($major_alert['data'][0]['title'])) {
    print $major_alert['data'][0]['title'];
  }
  ?>" />
  <br /><br />
  Body:<br />
  <textarea name="major_alert_body" cols="60" rows="10"><?php
  if (isset($major_alert['data'][0]['body'])) {
    print $major_alert['data'][0]['body'];
  }
  ?></textarea>
  <br /><br />
  Sidebar:<br />
  <textarea name="major_alert_sidebar" cols="60" rows="10"><?php
  if (isset($major_alert['data'][0]['body'])) {
    print $major_alert['data'][0]['sidebar'];
  }
  ?></textarea>
  <br /><br />
  Status:<br />
  <select name="major_alert_status">
    <?php if (isset($major_alert['data'][0]['status'])) : ?>
      <?php if ($major_alert['data'][0]['status'] == 1) : ?>
        <option value="1" selected="selected">Publish</option>
      <?php else : ?>
        <option value="1">Publish</option>
      <?php endif; ?>
    <?php else : ?>
      <option value="1">Publish</option>
    <?php endif; ?>

    <?php if (isset($major_alert['data'][0]['status'])) : ?>
      <?php if ($major_alert['data'][0]['status'] == 0) : ?>
        <option value="0" selected="selected">Unpublish</option>
      <?php else : ?>
        <option value="0">Unpublish</option>
      <?php endif; ?>
    <?php else : ?>
      <option value="0">Unpublish</option>
    <?php endif; ?>
  </select>
  <br /><br />
  <input type="submit" name="major_alert_submit_button" value="Submit">
</form>
